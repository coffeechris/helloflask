from .. import app
from ..services.hello_service import IndexService, SearchService

search_service = SearchService(app.config['ELASTICSEARCH_GREETING_INDEX'])
index_service = IndexService(app.config['ELASTICSEARCH_GREETING_INDEX'])
