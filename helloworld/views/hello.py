from .. import app


@app.route('/')
def hello_world():
    return 'Hello views package'


@app.route('/bye')
def good_bye():
    return 'see ya'
