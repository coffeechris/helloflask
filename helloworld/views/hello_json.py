from flask import jsonify, request

from .. import app
from ..models.hello_model import Greeting
from . import index_service, search_service

@app.route('/hello_json')
def hello_json():
    greeting = Greeting()
    greeting.message = 'hello world'
    greeting.to = 'Batman'

    return jsonify(greeting.__dict__)


@app.route('/search')
def search():
    docid = request.args.get('docid')
    return jsonify(search_service.get_by_id(docid))


@app.route('/index')
def index():
    greeting = Greeting()
    greeting.message = request.args.get('message')
    greeting.to = request.args.get('to')

    docid = request.args.get('docid')

    index_service.add_doc(docid=docid, greeting=greeting)

    return jsonify({'success': True})
