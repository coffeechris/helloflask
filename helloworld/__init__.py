from flask import Flask

app = Flask(__name__)
app.config.from_object('config')

from .models import hello_model
from .services import hello_service
from .views import hello
from .views import hello_json
