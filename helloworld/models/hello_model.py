class Greeting:
    def __init__(self):
        self.message = ''
        self.to = ''

    def __eq__(self, other):
        """Overrides the default implementation"""
        if isinstance(other, Greeting):
            return self.message == other.message and self.to == other.to
        return False
