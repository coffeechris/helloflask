from elasticsearch import Elasticsearch

from ..models.hello_model import Greeting

class SearchService:
    def __init__(self, index):
        self.es = Elasticsearch()
        self.index = index

    def get_by_id(self, docid):
        res = self.es.get(index=self.index, doc_type='greeting', id=docid)
        return res['_source']


class IndexService:
    def __init__(self, index):
        self.es = Elasticsearch()
        self.index = index

    def add_doc(self, docid: str, greeting: Greeting):
        self.es.index(index=self.index, doc_type='greeting', id=docid, body={
            'to': greeting.to,
            'message': greeting.message
        })
