from unittest import TestCase

from helloworld.models.hello_model import Greeting


class TestGreeting(TestCase):
    def test_eq(self):
        greeting1 = Greeting()
        greeting1.message = 'message 1'
        greeting1.to = 'to 1'

        greeting2 = Greeting()
        greeting2.message = 'message 1'
        greeting2.to = 'to 1'

        self.assertEqual(greeting1, greeting2)

    def test_neq(self):
        greeting1 = Greeting()
        greeting1.message = 'message 1'
        greeting1.to = 'to 1'

        greeting2 = Greeting()
        greeting2.message = 'message 2'
        greeting2.to = 'to 2'

        self.assertNotEqual(greeting1, greeting2)

