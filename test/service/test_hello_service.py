from unittest import TestCase
from unittest.mock import MagicMock

from helloworld.models.hello_model import Greeting
from helloworld.services.hello_service import IndexService, SearchService

class TestSearchService(TestCase):
    def test_get_by_id(self):
        search_service = SearchService('test index')
        search_service.es.get = MagicMock(return_value={
            '_source': 'test source'
        })

        self.assertEqual('test source', search_service.get_by_id('1'))
        search_service.es.get.assert_called_with(index='test index', doc_type='greeting', id='1')


class TestIndexService(TestCase):
    def test_add_doc(self):
        index_service = IndexService('test_index')
        index_service.es.index = MagicMock()

        greeting = Greeting()
        greeting.message = 'test message'
        greeting.to = 'test to'

        index_service.add_doc(docid='2',greeting=greeting)
        index_service.es.index.assert_called_with(index='test_index', doc_type='greeting', id='2', body={
            'to': 'test to',
            'message': 'test message'
        })
