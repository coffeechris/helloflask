from unittest import TestCase
from unittest.mock import MagicMock

import helloworld
from helloworld import app
from helloworld.models.hello_model import Greeting


class TestHelloJSON(TestCase):
    def test_search(self):
        client = app.test_client()
        helloworld.views.search_service.get_by_id = MagicMock(return_value={
            "message": "test message",
            "to": "test to"
        })

        self.assertEqual(b'{"message":"test message","to":"test to"}\n', client.get('/search?docid=1').data)
        helloworld.views.search_service.get_by_id.assert_called_with('1')

    def test_index(self):
        client = app.test_client()
        helloworld.views.index_service.add_doc = MagicMock()

        client.get('/index?message=test_message&to=test_to&docid=test_docid')

        greeting = Greeting()
        greeting.message = 'test_message'
        greeting.to = 'test_to'
        helloworld.views.index_service.add_doc.assert_called_with(docid='test_docid', greeting=greeting)
