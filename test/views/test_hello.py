from unittest import TestCase

from helloworld.views.hello import hello_world, good_bye


class TestHello(TestCase):
    def test_hello_world(self):
        self.assertEqual('Hello views package', hello_world())

    def test_good_bye(self):
        self.assertEqual('see ya', good_bye())
