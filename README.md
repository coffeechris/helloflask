# Overview
Just me playing around with Python and Flask.

# Setup
1. Set the following environment variables for local execution
    1. FLASK_ENV=development
        * enables debugger and automatic reloader
1. Install and run Elasticsearch 6
    * https://www.elastic.co/products/elasticsearch

# Command Line
1. Virtual Environment
    * Needs to be active in order to run things locally
        * `source venv/bin/activate`
    * When you are done, don't forget to deactivate it
        * `deactivate`
1. Run all unit tests
    * `python -m unittest`
1. Run flask app
    ```bash
    export PYTHONUNBUFFERED=1
    export FLASK_ENV=development
    python run.py
    ```

# URLs
1. Index a document
    * `http://localhost:5000/index?message=batman&to=robin&docid=test2`
    * Parameters
        * message
        * to
        * docid
1. Get a document by id
    * `http://localhost:5000/search?docid=test2`
    * Parameters
        * docid

# Troubleshooting
1. In case your dependencies are not reflected in requirements.txt, it has become stale, run this command:
    * `pip freeze > requirements.txt`

# References
* http://exploreflask.com/en/latest/index.html
* http://flask.pocoo.org/docs/1.0/
* https://www.jetbrains.com/help/pycharm/managing-dependencies.html
